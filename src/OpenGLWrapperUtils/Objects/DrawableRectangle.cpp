#include "DrawableRectangle.h"
#include "../Shape/RectShape.h"



DrawableRectangle::DrawableRectangle(const glm::vec2& siz, const glm::vec2& pos, const int& vertPosId, Shader *sh):
createdShape(true){
DrawableRectangle::rectsh =  new RectShape( vertPosId);
DrawableRectangle::obj = new DrawableObject2D(DrawableRectangle::rectsh, sh);
DrawableRectangle::obj->setSize(siz);
DrawableRectangle::obj->setPosition(pos);

}

DrawableRectangle::DrawableRectangle(const glm::vec2& siz, const glm::vec2& pos, Shape* shape, Shader *sh):
rectsh(shape), createdShape(false){
DrawableRectangle::obj = new DrawableObject2D(DrawableRectangle::rectsh, sh);
DrawableRectangle::obj->setSize(siz);
DrawableRectangle::obj->setPosition(pos);
}

#ifndef TEXTUREDDRAWABLERECTANGLE_H
#define TEXTUREDDRAWABLERECTANGLE_H
#include "DrawableRectangle.h"
#include "../Shape/TexturedRectShape.h"

class TexturedDrawableRectangle : public DrawableRectangle{

public:
 TexturedDrawableRectangle(const glm::vec2& siz, const glm::vec2& pos, const int& vertPosId  = DefaultShader2D::getVertPosId(), const int& vertTexId  = DefaultShader2D::getTexPosId(), Shader *sh = DefaultShader2D::getShader()):
 DrawableRectangle(siz, pos, new TexturedRectShape(vertPosId, vertTexId), sh){}

};

#endif

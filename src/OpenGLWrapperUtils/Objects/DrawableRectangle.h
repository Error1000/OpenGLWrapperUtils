#ifndef DRAWABLERECTANGLE_H
#define DRAWABLERECTANGLE_H

#include <OpenGLWrapper/renderEngine/Objects/DrawableObject2D.h>
#include "../Shape/RectShape.h"
#include "../Shader/DefaultShader2D.h"

class DrawableRectangle{
private:
    Shape *rectsh;
    bool createdShape;
    DrawableObject2D *obj;

public:
    DrawableRectangle(const glm::vec2& siz, const glm::vec2& pos, const int& vertPosId = DefaultShader2D::getVertPosId(), Shader *sh = DefaultShader2D::getShader());
    DrawableRectangle(const glm::vec2& siz, const glm::vec2& pos, Shape* shape, Shader *sh = DefaultShader2D::getShader());

   ~DrawableRectangle(){delete DrawableRectangle::obj; if(createdShape)delete DrawableRectangle::rectsh;}

    inline void bindObjectShader(){ DrawableRectangle::obj->bindObjectShader(); }
    inline void bindObjectShape(){ DrawableRectangle::obj->bindObjectShape(); }
    inline void renderObject(){ DrawableRectangle::obj->renderObject(); }
    inline void unbindObjectShader(){ DrawableRectangle::obj->unbindObjectShader(); }
    inline void unbindObjectShape(){ DrawableRectangle::obj->unbindObjectShape(); }

    inline DrawableObject2D* getObject(){ return DrawableRectangle::obj;}
    inline Shape* getShape(){ return DrawableRectangle::rectsh; }
    inline Shader* getShader(){ return DrawableRectangle::obj->getShader(); }
    inline glm::mat3& getModelMatrix(){ return DrawableRectangle::obj->getModelMatrix(); }
    inline void setObject(DrawableObject2D *newObj){ DrawableRectangle::obj = newObj;}
    inline void setShape(Shape *sh){ DrawableRectangle::rectsh = sh; }

    inline void move(const glm::vec2& dist){ DrawableRectangle::obj->move(dist);}
    inline void rotate(const float angle){ DrawableRectangle::obj->rotate(angle); }
    inline const float getRotation(){ return DrawableRectangle::obj->getRotation(); }
    inline void setRotation(const float angle){ DrawableRectangle::obj->setRotation(angle); }
    inline const glm::vec2 getSize(){ return DrawableRectangle::obj->getSize(); }
    inline void setSize(const glm::vec2& siz){ DrawableRectangle::obj->setSize(siz); }
    inline const glm::vec2 getPosition(){ return DrawableRectangle::obj->getPosition(); }
    inline void setPosition(const glm::vec2& pos){DrawableRectangle::obj->setPosition(pos); }

};


#endif

#ifndef DEFAULTSHADER_H
#define DEFAULTSHADER_H

#include <OpenGLWrapper/renderEngine/Rendering/Shader.h>

class DefaultShader2D{
typedef unsigned char byte;

private:
//Variable defintions
static Shader* sh;
const static std::string vSh, fSh;

public:
static void init();
static void destroy();
static Shader* getShader(){ return DefaultShader2D::sh; }
static void bindShader(){ DefaultShader2D::sh->bindShader(); }
static void unbindShader(){ DefaultShader2D::sh->unbindShader(); }
static void setShader(Shader* shader){ DefaultShader2D::sh = shader; }

static int getTransformationMatrixId();
static int getVertPosId();
static int getTexPosId();
static void useTexture(const bool useTex);
static void setSampler(const int sampler);
static void setRenderingColor(const byte& r, const byte& g, const byte& b);
static void setRenderingColor(const byte& r, const byte& g, const byte& b, const byte& a);

};

#endif

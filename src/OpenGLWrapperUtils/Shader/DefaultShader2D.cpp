#include "DefaultShader2D.h"

using std::string;

//Variable declarations
Shader* DefaultShader2D::sh;

const string DefaultShader2D::vSh =
string("//Set the current version so that opengl knows which version of the compiler to use to compile this\n")+
string("#version 330 core\n")+
string("\n")+
string("//Input\n")+
string("//Get the variables from the Shader class that compiled it\n")+
string("//Note it must have the same name as the name placed in the Shader class\n")+
string("layout(location = 0)in vec2 positions;\n")+
string("layout(location = 1)in vec2 tex_coord;\n")+
string("\n")+
string("//Output\n")+
string("//Shader variables\n")+
string("out vec2 pass_tex_coord;\n")+
string("\n")+
string("//Make a uniform for other to set\n")+
string("uniform mat3 transformationMatrix;\n")+
string("\n")+
string("void main() {\n")+
string("   //For fragment shader\n")+
string("   pass_tex_coord = tex_coord;\n")+
string("   //This is what we render\n")+
string("   gl_Position = vec4( transformationMatrix * vec3(positions, 1), 1 );\n")+
string("\n")+
string("}\n");


const string DefaultShader2D::fSh =
string("//Set the current version so that opengl knows which version of the compiler to use to compile this\n")+
string("#version 330 core\n")+
string("\n")+
string("//Input\n")+
string("//Note this time we don't get the vertices attribute since when we give the data we only give it to the vertex shader\n")+
string("//But what we do need to make is the uniform that we will use\n")+
string("uniform float r,g,b,a;\n")+
string("uniform bool useTexture;\n")+
string("uniform sampler2D objTexture;\n")+
string("\n")+
string("//Shader variables\n")+
string("//Input\n")+
string("in vec2 pass_tex_coord;\n")+
string("//Output\n")+
string("out vec4 FragmentColor;\n")+
string("\n")+
string("void main() {\n")+
string("   //Set the color of the quad\n")+
string("    if(useTexture != true){\n")+
string("     FragmentColor =  vec4( r, g, b, a);\n")+
string("    }else{\n")+
string("     FragmentColor = texture(objTexture, pass_tex_coord);\n")+
string("    }\n")+
string("\n")+
string("}\n")+
string("\n");





void DefaultShader2D::init(){
DefaultShader2D::sh = new Shader(DefaultShader2D::vSh, DefaultShader2D::fSh);
DefaultShader2D::sh->loadUniformId("transformationMatrix");
DefaultShader2D::sh->loadAttributeId("positions");
DefaultShader2D::sh->loadAttributeId("tex_coord");
DefaultShader2D::sh->loadUniformId("r");
DefaultShader2D::sh->loadUniformId("g");
DefaultShader2D::sh->loadUniformId("b");
DefaultShader2D::sh->loadUniformId("a");
DefaultShader2D::sh->loadUniformId("useTexture");
DefaultShader2D::sh->loadUniformId("objTexture");
}



void DefaultShader2D::destroy(){
   delete DefaultShader2D::sh;
}

int DefaultShader2D::getTransformationMatrixId(){
    return DefaultShader2D::sh->getUniformId("transformationMatrix");
}

int DefaultShader2D::getVertPosId(){
   return DefaultShader2D::sh->getAttributeId("positions");
}

int DefaultShader2D::getTexPosId(){
   return DefaultShader2D::sh->getAttributeId("tex_coord");
}


void DefaultShader2D::useTexture(const bool useTex){
DefaultShader2D::sh->setUniform(DefaultShader2D::sh->getUniformId("useTexture"), (useTex ? GL_TRUE : GL_FALSE) );
}

void DefaultShader2D::setSampler(const int sampler){
DefaultShader2D::sh->setUniform(DefaultShader2D::sh->getUniformId("objTexture"), sampler);
}

void DefaultShader2D::setRenderingColor(const byte& r, const byte& g, const byte& b){
DefaultShader2D::sh->setUniform(DefaultShader2D::sh->getUniformId("r"), (float) r/255.0f);
DefaultShader2D::sh->setUniform(DefaultShader2D::sh->getUniformId("g"), (float) g/255.0f);
DefaultShader2D::sh->setUniform(DefaultShader2D::sh->getUniformId("b"), (float) b/255.0f);
DefaultShader2D::sh->setUniform(DefaultShader2D::sh->getUniformId("a"), (float) 255.0f/255.0f);
}

void DefaultShader2D::setRenderingColor(const byte& r, const byte& g, const byte& b, const byte& a){
DefaultShader2D::sh->setUniform(DefaultShader2D::sh->getUniformId("r"), (float) r/255.0f);
DefaultShader2D::sh->setUniform(DefaultShader2D::sh->getUniformId("g"), (float) g/255.0f);
DefaultShader2D::sh->setUniform(DefaultShader2D::sh->getUniformId("b"), (float) b/255.0f);
DefaultShader2D::sh->setUniform(DefaultShader2D::sh->getUniformId("a"), (float) a/255.0f);
}

#ifndef TEXTUREDRECTSHAPE_H
#define TEXTUREDRECTSHAPE_H
#include <OpenGLWrapper/renderEngine/Shape/TexturedShape.h>

class TexturedRectShape : public TexturedShape{
public:
     //Variable definitions
     const static unsigned int posLen;
     const static float defaultPositions[];

     const static unsigned int indicesLen;
     const static int defaultIndices[];

     const static unsigned int texCoordsLen;
     const static float defaultTextureCoords[];


    //Function declaration & definition
    explicit TexturedRectShape(const int vertPosAttribIndex, const int vertTextureCoordsAttribIndex)
    : TexturedShape(TexturedRectShape::defaultPositions, TexturedRectShape::posLen, vertPosAttribIndex, 2, GL_STATIC_DRAW, TexturedRectShape::defaultTextureCoords, TexturedRectShape::texCoordsLen, vertTextureCoordsAttribIndex, 2, GL_STATIC_DRAW, TexturedRectShape::defaultIndices, TexturedRectShape::indicesLen, GL_STATIC_DRAW){}

    explicit TexturedRectShape(const int vertPosAttribIndex, const int vertTextureCoordsAttribIndex, const float* textureCoords, const int textureCoordsLen)
    : TexturedShape(TexturedRectShape::defaultPositions, TexturedRectShape::posLen, vertPosAttribIndex, 2, GL_STATIC_DRAW, textureCoords, textureCoordsLen, vertTextureCoordsAttribIndex, 2, GL_STATIC_DRAW, TexturedRectShape::defaultIndices, TexturedRectShape::indicesLen, GL_STATIC_DRAW){}


};

#endif

#include "RectShape.h"

//Variable declarations
const unsigned int RectShape::posLen = 8;
const unsigned int RectShape::indicesLen = 6;


const float RectShape::defaultPositions[RectShape::posLen] = {
                                     //Nr.
			//TOP RIGHT TRIANGLE
			-1, 1, //TOP LEFT          0
			1, 1, //TOP RIGHT         1
			1, -1, // BOTTOM RIGHT     2

			//BOTTOM LEFT TRIANGLE
			-1, -1 // BOTTOM LEFT      3

};


const int RectShape::defaultIndices[RectShape::indicesLen] = {
         //TOP RIGHT TRIANGLE
			0, //TOP LEFT
			1, //TOP RIGTH
			3, //BOTTOM LEFT

			//BOTOM LEFT TRIANGLE
			1, //TOP RIGHT
			2, //BOTTOM RIGHT
			3 //BOTTOM LEFT

};

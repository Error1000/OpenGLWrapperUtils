#ifndef RECTSHAPE_H
#define RECTSHAPE_H
#include <OpenGLWrapper/renderEngine/Shape/Shape.h>

class RectShape : public Shape{
public:
     //Variable defintions
     const static unsigned int posLen;
     const static float defaultPositions[];

     const static unsigned int indicesLen;
     const static int defaultIndices[];



    //Function declaration & definition
    explicit RectShape(const int vertPosAttribIndex)
    : Shape(RectShape::defaultPositions, RectShape::posLen, vertPosAttribIndex, 2, GL_STATIC_DRAW, RectShape::defaultIndices, RectShape::indicesLen, GL_STATIC_DRAW){}



};

#endif

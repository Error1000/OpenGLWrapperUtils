#include "TexturedRectShape.h"

//Variable declarations
const unsigned int TexturedRectShape::posLen = 8;
const unsigned int TexturedRectShape::indicesLen = 6;
const unsigned int TexturedRectShape::texCoordsLen = 8;

const float TexturedRectShape::defaultPositions[TexturedRectShape::posLen] = {
                                     //Nr.
			//TOP RIGHT TRIANGLE
			-1, 1, //TOP LEFT          0
			1, 1, //TOP RIGHT         1
			1, -1, // BOTTOM RIGHT     2

			//BOTTOM LEFT TRIANGLE
			-1, -1 // BOTTOM LEFT      3

};


const int TexturedRectShape::defaultIndices[TexturedRectShape::indicesLen] = {
         //TOP RIGHT TRIANGLE
			0, //TOP LEFT
			1, //TOP RIGTH
			3, //BOTTOM LEFT

			//BOTOM LEFT TRIANGLE
			1, //TOP RIGHT
			2, //BOTTOM RIGHT
			3 //BOTTOM LEFT

};


const float TexturedRectShape::defaultTextureCoords[TexturedRectShape::texCoordsLen] = {
             //Nr.
			//TOP RIGHT TRIANGLE
			0, 0, //TOP LEFT VERTEX OF PHOTO         0
			1, 0, //TOP RIGHT VERTEX OF PHOTO        1
			1, 1, // BOTTOM RIGHT VERTEX OF PHOTO    2

			//BOTOM LEFT TRIANGLE
			0, 1 // BOTTOM LEFT VERTEX OF PHOTO 3
};

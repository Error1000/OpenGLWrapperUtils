#include "Camera2D.h"
#include <glm/gtx/matrix_transform_2d.hpp>


#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

using glm::vec2;
using glm::vec3;
using glm::mat3;

//Declarations
mat3 Camera2D::cameraViewMatrix(1);
vec2 Camera2D::pos = vec2(0, 0);
bool Camera2D::gototarget = false;


void Camera2D::setPosition(const vec2& newPos){
    //Revert positon
    Camera2D::cameraViewMatrix = glm::translate(Camera2D::cameraViewMatrix, Camera2D::pos );
    //And move to new position
    Camera2D::pos = newPos;
    Camera2D::cameraViewMatrix = glm::translate(Camera2D::cameraViewMatrix, -Camera2D::pos );

}


void Camera2D::followObjectBetweenBounds(const DrawableObject2D* obj, const vec2& minPos, const vec2& maxPos, const float snapToTargetRange, const vec2& lerpSpeed){

vec2 target = Camera2D::translatePosRelativeToCamera(obj->getPosition());
if(target.x >= maxPos.x || target.y >= maxPos.y ||
   target.x <= minPos.x || target.y <= minPos.y){
   Camera2D::gototarget = true;
}

if(gototarget){
    if(Camera2D::getPosition() != obj->getPosition()){
        //Move to target
        Camera2D::move( (obj->getPosition() - Camera2D::getPosition()) * lerpSpeed );
        if( abs(obj->getPosition().x - Camera2D::getPosition().x) < snapToTargetRange &&
            abs(obj->getPosition().y - Camera2D::getPosition().y) < snapToTargetRange){

              Camera2D::setPosition(obj->getPosition());
        }

    }else{
        //Already at target
     Camera2D::gototarget = false;
    }
}

}



void Camera2D::followObjectBetweenBoundsOnX(const DrawableObject2D* obj, const float minPos, const float maxPos, const float snapToTargetRange, const float lerpSpeed){

vec2 target = Camera2D::translatePosRelativeToCamera(obj->getPosition());
if(target.x >= maxPos ||
   target.x <= minPos){
   Camera2D::gototarget = true;
}

if(gototarget){
    if(Camera2D::getPosition().x != obj->getPosition().x){
        //Move to target
        Camera2D::move( vec2((obj->getPosition().x - Camera2D::getPosition().x) * lerpSpeed, 0) );
        if( abs(obj->getPosition().x - Camera2D::getPosition().x) < snapToTargetRange)
            Camera2D::setPosition( vec2(obj->getPosition().x, Camera2D::getPosition().y) );
    }else{
        //Already at target
     Camera2D::gototarget = false;
    }
}


}


void Camera2D::followObjectBetweenBoundsOnY(const DrawableObject2D* obj, const float minPos, const float maxPos, const float snapToTargetRange, const float lerpSpeed){

vec2 target = Camera2D::translatePosRelativeToCamera(obj->getPosition());
if(target.y >= maxPos ||
   target.y <= minPos){
   Camera2D::gototarget = true;
}

if(gototarget){
    if(Camera2D::getPosition().y != obj->getPosition().y){
        //Move to target
        Camera2D::move( vec2(0, (obj->getPosition().y - Camera2D::getPosition().y) * lerpSpeed) );
        if( abs(obj->getPosition().y - Camera2D::getPosition().y) < snapToTargetRange)
            Camera2D::setPosition( vec2(Camera2D::getPosition().x, obj->getPosition().y) );
    }else{
        //Already at target
     Camera2D::gototarget = false;
    }
}

}


void Camera2D::move(const vec2& dist){
  Camera2D::cameraViewMatrix = glm::translate(Camera2D::cameraViewMatrix, -dist);
  Camera2D::pos = Camera2D::pos + dist;
}


vec2 Camera2D::getPosition(){
  return Camera2D::pos;
}

vec2 Camera2D::translatePosRelativeToCamera(const vec2& objPos){
  return objPos - Camera2D::pos;
}

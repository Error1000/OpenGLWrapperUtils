#ifndef CAMERA_H
#define CAMERA_H

#include <glm/glm.hpp>
#include <OpenGLWrapper/Window/Window.h>
#include <OpenGLWrapper/renderEngine/Objects/DrawableObject2D.h>

class Camera2D{
private:
    static glm::mat3 cameraViewMatrix;
    static glm::vec2 pos;
    static bool gototarget;

public:
    static inline glm::mat3 getViewMatrix(){ return Camera2D::cameraViewMatrix; }

    static void setPosition(const glm::vec2& pos);
    static glm::vec2 getPosition();
    static void followObjectBetweenBounds(const DrawableObject2D* obj, const glm::vec2& minPos, const glm::vec2& maxPos, const float snapToTargetRange = 0.2f, const glm::vec2& lerpSpeed = glm::vec2(Window::getAverageDelta() * 5, Window::getAverageDelta() * 5));
    static void followObjectBetweenBoundsOnX(const DrawableObject2D* obj, const float minPos, const float maxPos, const float snapToTargetRange = 0.2f, const float lerpSpeed = Window::getAverageDelta() * 5);
    static void followObjectBetweenBoundsOnY(const DrawableObject2D* obj, const float minPos, const float maxPos, const float snapToTargetRange = 0.2f, const float lerpSpeed = Window::getAverageDelta() * 5);

    static glm::vec2 translatePosRelativeToCamera(const glm::vec2& pos);
    static void move(const glm::vec2& dist);
    static void init();
};

#endif

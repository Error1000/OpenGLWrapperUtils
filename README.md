# OpenGLWrapperUtils
This is a simple "add-on" to the OpenGLWrapper project that is used in many of my other project to make the creation of code easier.

NOTE: This git repository uses submoduels so to clone it you need to add the arguments: --recurse-submodules -j8 to the git clone command!!!
NOTE: Since this repostiory uses submodules DO NOT download it using the web interface it does not downlaod submodules isntead clone the project with the arguments: --recurse-submodules -j8 !!!

# How to build the release and get libraries

  Note: The automatron commands must be run from the project folder where this README is located!!!

  - Download and compile the release of my OpenGLWrapper project (make sure the directory in which that project is stored is the same as the one this one is stored in)
  
  Note: Be sure to remove the -master after the name of the folders the projects are stored in
  
  # GNU/Linux (using make to build and the default package manager to install stuff and assuming that the right libraries are installed from the fact that the user has to have the OpenGLWrapper project compiled for which he needs to install the libraries)
  
   - Ubuntu: ./automatron9000/automatron9000.sh -m release -o ubuntu

   - Arch: ./automatron9000/automatron9000.sh -m release -o arch

  # MacOS (using make to build and the brew package manager to install stuff and assuming that the right libraries are installed from the fact that the user has to have the OpenGLWrapper project compiled for which he needs to install the libraries)
   Note: This command will download and install the required applications needed to build and install brew. 

   - ./automatron9000/automatron9000.sh -m release -o macos

  # Windows (using make to build and the chocolatey package manager to install stuff and using the libraries from the Libraries\windows folder from the OpenGLWrapper project folder)
   Note: This command will download and install the required applications needed to build and install chocolatey.
   
  - Open cmd as administrator
  - Run: automatron9000\automatron9000.bat -m release -o windows
   